//
//  Constants.swift
//  BCIT-COMP3912-Week07-RemoteVideoApp
//
//  Created by Massimo Savino on 2016-11-04.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import Foundation

struct Constants {
    
    static let OK = "OK"
    static let Cancel = "Cancel"
    
    struct Trainspotting {
        
        static let title = "T2 Trainspotting"
        static let updatedQuotes = "Choose life\nChoose Facebook, Twitter, Instagram and hope that someone, somewhere cares\nChoose looking up old flames, wishing you’d done it all differently\nAnd choose watching history repeat itself\nChoose your future\nChoose reality TV, slut shaming, revenge porn\nChoose a zero hour contract, a two hour journey to work\nAnd choose the same for your kids, only worse, and smother the pain with an unknown dose of an unknown drug made in somebody’s kitchen\nAnd then… take a deep breath\nYou’re an addict, so be addicted\nJust be addicted to something else\nChoose the ones you love\nChoose your future\nChoose life"
    }
    
    struct Errors {
        
        
    }
}
