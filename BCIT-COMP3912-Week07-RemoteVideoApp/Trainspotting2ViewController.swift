//
//  Trainspotting2ViewController.swift
//  BCIT-COMP3912-Week07-RemoteVideoApp
//
//  Created by Massimo Savino on 2016-11-04.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class Trainspotting2ViewController: UIViewController {

    // MARK: Properties
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var playerView: YTPlayerView!
    
    @IBOutlet weak var updatedQuotesLabel: UILabel!
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpUI()
    }

    
    override func viewDidAppear(_ animated: Bool) {
        
        updatedQuotesLabel.text = Constants.Trainspotting.updatedQuotes
        UIView.animate(withDuration: 5.0, delay: 0.5, options: .curveEaseInOut, animations: {
            self.updatedQuotesLabel.center.x -= self.view.bounds.width
        }, completion: nil)
    }
    
    
    // MARK: Setup functions
    
    func setUpUI() {
        
        titleLabel.text = Constants.Trainspotting.title
        playerView.load(withVideoId: "EsozpEE543w")
    }

}
